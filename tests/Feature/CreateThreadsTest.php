<?php

   namespace Tests\Feature;

   use App\Activity;
   use Illuminate\Foundation\Testing\DatabaseMigrations;
   use Tests\TestCase;
   use Illuminate\Foundation\Testing\WithFaker;
   use Illuminate\Foundation\Testing\RefreshDatabase;

   class CreateThreadsTest extends TestCase
   {
      use DatabaseMigrations;

      /** @test */
      public function guests_may_not_create_threads()
      {
         $this->withExceptionHandling();

         $this->get('/threads/create')
            ->assertRedirect('/login');

         $this->post('/threads')
            ->assertRedirect('/login');
      }

      /** @test */
      public function an_authenticated_user_can_create_new_forum_thread()
      {
         $this->signIn();

         $thread=make('App\Thread');

         $response=$this->post('/threads',$thread->toArray());

         $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
      }

      /** @test */
      public function a_thread_requires_a_title()
      {
         $this->publishThread(['title'=>NULL])
            ->assertSessionHasErrors('title');
      }

      /** @test */
      public function a_thread_requires_a_body()
      {
         $this->publishThread(['body'=>NULL])
            ->assertSessionHasErrors('body');
      }

      /** @test */
      public function a_thread_requires_a_valid_channel()
      {
         factory('App\Channel',2)->create();

         $this->publishThread(['channel_id'=>NULL])
            ->assertSessionHasErrors('channel_id');

         $this->publishThread(['channel_id'=>999])
            ->assertSessionHasErrors('channel_id');
      }

      /** @test */
      public function unauthorized_users_may_not_delete_threads()
      {
         $this->withExceptionHandling();

         $thread=create('App\Thread');

         $this->delete($thread->path())
            ->assertRedirect('/login');

         $this->signIn();

         $this->delete($thread->path())
            ->assertStatus(403);

      }

      /** @test */
      public function authorized_users_can_delete_threads()
      {
         $this->signIn();

         $thread=create('App\Thread',['user_id'=>auth()->id()]);
         $reply=create('App\Reply',['thread_id'=>$thread->id]);

         $response=$this->json('DELETE',$thread->path());

         $response->assertStatus(200);

         $this->assertDatabaseMissing('threads',['id'=>$thread->id]);
         $this->assertDatabaseMissing('replies',['id'=>$reply->id]);

         $this->assertEquals(0,Activity::count());

//         $this->assertDatabaseMissing('activities',[
//            'subject_id'=>$thread->id,
//            'subject_type'=>get_class($thread)
//         ]);
      }

      public function threads_may_only_be_deleted_by_those_how_have_permmision()
      {
         $this->signIn();

         $thread=create('App\Thread');
      }

      public function publishThread($override)
      {
         $this->signIn()->withExceptionHandling();

         $thread=make('App\Thread',$override);

         return $this->post('/threads',$thread->toarray());
      }
   }
