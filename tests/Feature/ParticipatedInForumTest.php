<?php

   namespace Tests\Feature;

   use Illuminate\Foundation\Testing\DatabaseMigrations;
   use Tests\TestCase;

   class ParticipatedInForumTest extends TestCase
   {
      use DatabaseMigrations;

      /** @test */
      public function unauthentication_user_may_not_add_replies()
      {
         $this->withExceptionHandling()
            ->post('/threads/some-channel/1/replies',[])
            ->assertRedirect('/login');
      }

      /** @test */
      public function an_authenticated_user_can_participated_in_forum_thread()
      {
         $this->signIn();

         $thread=create('App\Thread');

         $reply=make('App\Reply');
         $this->post($thread->path().'/replies',$reply->toArray());

         $this->assertDatabaseHas('replies',['body'=>$reply->body]);
         $this->assertEquals(1,$thread->fresh()->replies_count);
      }

      /** @test */
      public function a_reply_requires_a_body()
      {
         $this->withExceptionHandling()->signIn();

         $thread=create('App\Thread');
         $reply=make('App\Reply',['body'=>NULL]);

         $this->post($thread->path().'/replies',$reply->toArray())
            ->assertSessionHasErrors('body');


      }

      /** @test*/
      public function unauthurized_user_cannot_delete_reply()
      {
         $this->withExceptionHandling();

         $reply=create('App\Reply');

//         $this->delete("/replies/{$reply->id}")
//            ->assertRedirect('login');

         $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);
      }

      /** @test*/
      public function an_autherized_user_can_delete_replies()
      {
         $this->signIn();
         $reply=create('App\Reply',['user_id'=>auth()->id()]);

         $this->delete("/replies/{$reply->id}");

         $this->assertDatabaseMissing('replies',['id'=>$reply->id]);
         $this->assertEquals(0,$reply->thread->fresh()->replies_count);
      }

      /** @test*/
      public function unauthorized_user_cannot_update_replies()
      {
         $this->withExceptionHandling();

         $reply=create('App\Reply');

//         $this->patch("/replies/{$reply->id}")
//            ->assertRedirect('/login');

         $this->signIn()->patch("/replies/{$reply->id}")
            ->assertStatus(403);



      }

      /** @test*/
      public function authorized_user_can_update_replies()
      {
         $this->signIn();

         $reply=create('App\Reply',['user_id'=>auth()->id()]);

         $updateReply='this is for the tests, Update';

         $this->patch("/replies/{$reply->id}",[
            'body'=>$updateReply
         ]);

         $this->assertDatabaseHas('replies',[
            'id'=>$reply->id,
            'body'=>$updateReply
         ]);
      }
   }
