<?php

namespace Tests\Unit;

use App\Notifications\ThreadWasUpdated;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadTest extends TestCase {

    use DatabaseMigrations;
    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = create('App\Thread');
    }

    /** @test */
    public function a_thread_can_made_a_string_path()
    {
        $thread = create('App\Thread');

        $this->assertEquals("/threads/{$thread->channel->slug}/{$thread->id}", $thread->path());
    }

    /** @test */
    public function a_thread_has_replies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    /** @test */
    public function a_thread_has_creator()
    {
        $this->assertInstanceOf('App\User', $this->thread->creator);
    }

    /** @test */
    public function a_thread_can_add_a_reply()
    {
        $this->thread->addReply([
            'body' => 'Foo Bar',
            'user_id' => 1,
        ]);

        $this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    public function a_thread_belongs_to_a_channel()
    {
        $thread = create('App\Thread');

        $this->assertInstanceOf('App\Channel', $thread->channel);
    }

    /** @test*/
    public function a_thread_can_be_subscribed_to()
    {
        //Given we have a thread
        $thread=create('App\Thread');

        //when the user subscribes to the thread
        $thread->subscribe($userId = 1);

        //Then we should be able to fetch all threads that the user has subscribed to
        $this->assertEquals(
            1,
            $thread->subscriptions()->where('user_id',$userId)->count()
        );
    }

    /** @test*/
    public function a_thread_can_be_unsubscribed_from()
    {
        //Given we have a thread
        $thread=create('App\Thread');

        //And a user who is subscribe to the thread
        $thread->subscribe($userId = 1);

        $thread->unsubscribed($userId);
        $this->assertCount(0,$thread->subscriptions);
    }

    /** @test */
    public function is_knows_if_the_authenticated_user_subscribed_to_it()
    {
        $thread=create('App\Thread');

        $this->signIn();

        $this->assertFalse($thread->isSubscribedTo);

        $thread->subscribe();

        $this->assertTrue($thread->isSubscribedTo);

    }

    /** @test */
    public function a_thread_notifies_all_registerd_subscribes_when_a_reply_is_added()
    {
        Notification::fake();

        $this->signIn()
            ->thread->
            subscribe()
            ->addReply([
            'body'=>'Foo',
            'user_id'=>1
        ]);

        Notification::assertSentTo(auth()->user(),ThreadWasUpdated::class);
    }
}