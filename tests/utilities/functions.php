<?php
   function make($class,$attributes=[],$time=NULL)
   {
      return factory($class,$time)->make($attributes);
   }

   function create($class,$attributes=[],$time=NULL)
   {
      return factory($class,$time)->create($attributes);
   }
?>