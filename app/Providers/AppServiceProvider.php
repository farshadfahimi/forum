<?php

namespace App\Providers;

use App\Channel;
use Illuminate\Filesystem\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       Schema::defaultStringLength(200);

       \View::composer('*',function($view){
          $channels=\Cache::rememberForever('channels',function(){
               return Channel::all();
          });

         $view->with('channels',$channels);
       });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Not necessary for laravel 5.5 or higher
//       if($this->app->isLocal())
//          $this->app->register();
    }
}
