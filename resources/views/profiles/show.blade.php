@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card mb-5">
            <div class="card-header">
                <h3 class="card-title">
                    {{ $profileUser->name }}

                    <small>Since {{ $profileUser->created_at->diffForHumans()}}</small>
                </h3>
            </div>
        </div>

        @forelse($activities as $date => $activity)
            <h5>{{ $date }}</h5>
            <hr>
            @foreach($activity as $record)
                @if(view()->exists("profiles.activities.{$record->type}"))
                    @include("profiles.activities.{$record->type}",['activity'=>$record])
                @endif
            @endforeach
        @empty
            <p>There is no activities</p>
        @endforelse

    </div>
@endsection