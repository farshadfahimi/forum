@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @forelse($threads as $thread)
                    <div class="card mb-3">
                        <div class="card-header">
                            <a href="{{ $thread->path() }}">{{ $thread->title }}</a>

                            <a class="float-right btn btn-sm btn-secondary" href="{{ $thread->path() }}">
                                <span class="budget budget-light">{{ $thread->replies_count }} {{ str_plural('reply',$thread->replies_count) }}</span>
                            </a>
                        </div>

                        <div class="card-body">
                            {{ $thread->body }}
                        </div>
                    </div>
                @empty
                    <div class="alert alert-danger">
                        There is no thread in this Channel
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
