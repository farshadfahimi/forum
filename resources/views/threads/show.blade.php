@extends('layouts.app')

@section('content')
    <thread-view :initial-replies-count="{{ $thread->replies_count }}" inline-template>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card mb-5">
                        <div class="card-header">
                            <a href="{{ route('profile',$thread->creator) }}">
                                {{ $thread->creator->name }}
                            </a> posted:

                            {{ $thread->title }}


                            <div class="float-right">

                                @can('update',$thread)
                                    <form action="{{ $thread->path() }}" method="POST">
                                        {{ csrf_field() }}

                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                @endcan
                            </div>
                        </div>

                        <div class="card-body">
                            {{ $thread->body }}
                        </div>
                    </div>

                    <replies @added="repliesCount++"
                             @removed="repliesCount--">

                    </replies>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <p>
                                This Thread was Published as {{ $thread->created_at->diffForHumans() }} by

                                <a href="#">{{ $thread->creator->name }}</a> and currently

                                has <span v-text="repliesCount"></span> {{ str_plural('comment',$thread->replies_count) }}.
                            </p>

                            <div>
                                <subscribe-button :active="{{ json_encode($thread->isSubscribedTo) }}">Subscribe</subscribe-button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </thread-view>
@endsection
