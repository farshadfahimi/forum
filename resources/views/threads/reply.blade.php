<reply :attributes="{{ $reply }}" inline-template v-cloak>
    <div id="reply-{{$reply->id}}" class="card mb-3">
        <div class="card-header">
            <a href="{{ route('profile',$reply->owner) }}" class="float-left">
                {{ $reply->owner->name }}
            </a> said {{ $reply->created_at->diffForHumans() }}...

            @if(Auth::check())
                <favorite :reply="{{ $reply }}"></favorite>
            @endif
        </div>

        <div class="card-body">
            <div v-if="editing">
                <div class="form-group">
                    <textarea name="" class="form-control" v-model="body"></textarea>
                </div>

                <button class="btn btn-sm btn-primary" @click="update">Update</button>
                <button class="btn btn-sm btn-light" @click="editing = false">Cancel</button>

            </div>
            <div v-else v-text="body"></div>

        </div>

        @can('update',$reply)
            <div class="card-footer flex">
                <button class="btn btn-sm btn-secondary ml-2" @click="editing = true">Edit</button>

                <button class="btn btn-sm btn-danger" @click="destroy">Delete</button>
            </div>
        @endcan
    </div>
</reply>